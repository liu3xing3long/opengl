# Ubuntu 16.04 [![build status](https://gitlab.com/nvidia/opengl/badges/ubuntu16.04/build.svg)](https://gitlab.com/nvidia/opengl/commits/ubuntu16.04)

- [`base`, `base-ubuntu16.04` (*base/Dockerfile*)](https://gitlab.com/opengl/cuda/blob/ubuntu16.04/base/Dockerfile)
- [`1.0-glvnd-runtime`, `runtime`, `1.0-glvnd-runtime-ubuntu16.04` (*1.0-glvnd/runtime/Dockerfile*)](https://gitlab.com/nvidia/opengl/blob/ubuntu16.04/1.0-glvnd/runtime/Dockerfile)
- [`1.0-glvnd-devel`, `devel`, `1.0-glvnd-devel-ubuntu16.04` (*1.0-glvnd/devel/Dockerfile*)](https://gitlab.com/nvidia/opengl/blob/ubuntu16.04/1.0-glvnd/devel/Dockerfile)
